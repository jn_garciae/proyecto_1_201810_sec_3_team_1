package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.ILinkedList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Stack;
import model.vo.Compania;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	private LinkedList<Compania> companias;
	private Compania primera;
	
	
	public TaxiTripsManager(){
		
		companias = new LinkedList<Compania>();
	}
	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		boolean c = false;
		JsonParser jp = new JsonParser();
		Stack<Servicio> servicios = null;
		try
		{
			JsonReader j = new JsonReader(new FileReader(direccionJson));
			Gson g = new GsonBuilder().create();
			j.beginArray();
			while(j.hasNext())
			{
				Servicio s = g.fromJson(j, Servicio.class);

				servicios.push(s);
			}
			}
			
		
		catch(JsonIOException e)
		{
			e.printStackTrace();
		} catch(JsonSyntaxException o)
		{
			o.printStackTrace();
		}
		catch(FileNotFoundException f){
			f.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return c;
		// TODO Auto-generated method stub
		
	}

	
	public LinkedList<Compania> darCompanias(){
		return companias;
	}
	
	public Compania primera(){
		return primera;
	}
	
	@Override
	public String[] darInformacionTaxiEnRango(String id, String inicial, String f) throws ParseException {
		
		boolean encontro = false;
		String[] arreglo = new String[3];
		Taxi en = null;

		for(int i =0; i<companias.size()&&!encontro;i++){

			if(companias.get(i).buscarTaxiId(id)!=null){

				en = companias.get(i).buscarTaxiId(id);
				arreglo[0] = en.getCompany();
				arreglo[1] = ""+en.gananciaServicio(inicial, f);
				arreglo[2]=""+en.serviciosEnRango(inicial, f).size();
				arreglo[3]= ""+ en.distanciaServicio(inicial, f);
				encontro = true;

			}
		}

		return arreglo;


	}

	@Override
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(String dateI, String dateF, String company) throws ParseException {
		
		boolean encontro = false;
		Taxi retornar = null;
		for(int i=0; i<companias.size()&& !encontro;i++){
			
			if(companias.get(i).equals(company)){
				
				retornar = companias.get(i).taxiMayorServicios(dateI, dateF);
			}
		}
		
		return retornar;
	}

	@Override
	public IQueue<Servicio> darServiciosEnPeriodo(String dateI, String dateF) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LinkedList<Servicio> darListaRangosDistancia(String horaInicial, String horaFinal) throws ParseException {
		
		LinkedList<Servicio> listaRangos = new LinkedList<Servicio>();
		for(int i=0; i<companias.size();i++){
		  
			LinkedList<Servicio> x = companias.get(i).taxisRango(horaInicial, horaFinal);
			
			for(int j=0; j<companias.size();j++){
				
				for(int g=0; g<listaRangos.size();g++){
					
					
				}
			}
			
			
		}
		
		return null;
	}
	

	@Override
	public LinkedList<Compania> darCompaniasTaxisInscritos() {
		// TODO Auto-generated method stub
		LinkedList<Compania> companiasTaxi = null;
		for(int i = 0; i<companias.size(); i++)
		{
			Compania actual = companias.get(i);
			if(actual.getTaxisInscritos().size() != 0)
			{
				companiasTaxi.add(actual);
			}
		
		}
		return companiasTaxi;
		
	}

	@Override
	public Taxi darTaxiMayorFacturacion(String dateI, String dateF, String nomCompania) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Servicio[] darServiciosZonaValorTotal(String dateI, String dateF, String idZona) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LinkedList<ZonaServicios> darZonasServicios(String dateI, String dateF) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LinkedList<Compania> companiasMasServicios(String dateI, String dateF, int n) throws ParseException {
		// TODO Auto-generated method stub

		double ultimoAgregado = 0;
		Compania ultimoAgregada = null;
		LinkedList<Compania> lista = new LinkedList<Compania>();
		

		for(int i=0; i<companias.size();i++){

			int x = companias.get(i).serviciosIniciados(dateI, dateF);
			if(x>ultimoAgregado){

				if(lista.size()<=n){

					ultimoAgregado = x;
					lista.add(companias.get(i));
					ultimoAgregada = companias.get(i);
				}

				else{
					lista.delete(ultimoAgregada);
					lista.add(companias.get(i));
					ultimoAgregada = companias.get(i);

				}
			}
		}

		return lista;
	}

	@Override
	public LinkedList<Taxi> taxisMasRentables() {
		
		LinkedList<Taxi> lista = new LinkedList<Taxi>();
		
		for(int i=0; i<companias.size();i++){
			
			lista.add(companias.get(i).taxiMasRentable());
		}
		
		return lista;
	}

	@Override
	public IStack<Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILinkedList<Taxi> darListaTaxi() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILinkedList<Taxi> taxisRango(String dateI, String dateF) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date fecha(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	

}
