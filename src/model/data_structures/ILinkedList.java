package model.data_structures;


/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */

public interface ILinkedList<T extends Comparable<T>> {
	
	/**
	 * Agrega un nuevo elemento a la lista
	 */
	public void add(T elemento);
	
	/**
	 * Elimina el elemento recibido por parametro 
	 */
	
	public void delete(T elemento);
	
	/**
	 * Retorna el elemento en la posicion recibida por parametro 
	 * @param pPosicion
	 * @return T elemento, null si no existe 
	 */
	
	public T get(int pPosicion);
	
	/**
	 * Retorna el tama�o de la lista 
	 * @return 
	 */
	
	public int size();
	
	/**
	 * Retorna el siguiente elemento de la lista 
	 * @param elemento
	 * @return el siguienre elemento en la lista 
	 */
	
	public T next(T elemento);
	
	/**
	 * 
	 * @return
	 */
	public T getCurrent();	
	
	/**
	 * Retorna el elemento recibido por parametro 
	 * @param elemento
	 * @return
	 */
	public T get(T elemento);

	

	
	


}
