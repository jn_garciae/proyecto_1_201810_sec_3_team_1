package model.data_structures;

import java.util.NoSuchElementException;

public class Stack<E> implements IStack<E>{

	
	Node<E> first;
	
	int longitud;
	
	@Override
	public void push(E item) {
		Node<E> oldfirst = first;
		first = new Node<E>(item);
		first.next = oldfirst;
		longitud++;
	}
	

	@Override
	public E pop() {
		if (isEmpty()) throw new NoSuchElementException("Stack underflow");
		E item = first.item;        
		first = first.next;            
		longitud--;
		return item;                  

	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first==null;
	}
	
	

	@Override
	public Iterator<E> iterador() {
		ListIterator<E> listIterator = new ListIterator<E>(first);
		return listIterator;
	}
	
	public int size(){
		return longitud;
	}

	

}
