package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StackTest<E> {

	private IStack<E> pila;
	
	E item;
	
	@Before
	public void setupEscenario1(){
		
		pila = new Stack<E>();
	}
	@Test
	public void push() {
	    
		setupEscenario1();
		
		pila.push(item);
		
		assertFalse("El objeto no fue inicializado", pila.isEmpty());
	}
	
	@Test
	public void pop() {
	    
		setupEscenario1();
		pila.push(item);
		E x = pila.pop();
		
		
		assertTrue("", pila.isEmpty());
	}
	
	@Test
	public void isEmpty(){
		
		assertTrue("", pila.isEmpty());
	}
	
	@Test
	public void size(){
		
	
	pila.push(item);
	
	assertEquals("",1,pila.size());

	pila.pop();
	
	assertEquals("",0, pila.size());
	}
	
	


}
