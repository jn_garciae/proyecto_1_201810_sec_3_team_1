package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LinkedListTest<T extends Comparable<T>> {

	
private LinkedList<T> lista;
	
	T item;
	
	@Before
	public void setupEscenario1(){
		
		lista = new LinkedList<T>();
	}
	@Test
	public void add() {
	    
		setupEscenario1();
		
		lista.add(item);
		
		assertNotNull("El objeto no fue inicializado", lista.primero);
	}
	
	@Test
	public void delete() {
	    
		setupEscenario1();
		lista.add(item);
		lista.delete(item);
		
		
		assertNull("", lista.primero);
	}
	
	
	
	@Test
	public void size(){
		
	
	lista.add(item);
	
	assertEquals("",1,lista.size());

	lista.delete(item);
	
	assertEquals("",0, lista.size());
	}

}
