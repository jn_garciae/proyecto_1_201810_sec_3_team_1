package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;

public class QueueTest<T> {

	/**
	 * 
	 */
	private Queue<T> cola;
	private T item;
    private T item2;
	
	@Before
	public void setupEscenario1(){
		
		cola = new Queue<T>();
	}
	@Test
	public void enqueue() {
	    
		setupEscenario1();
		
		cola.enqueue(item);
		
		assertFalse("El objeto no fue inicializado", cola.isEmpty());
	}
	
	@Test
	public void dequeue() {
	    
		setupEscenario1();
		cola.enqueue(item);
		T x = cola.dequeue();
		
		
		assertNull("", cola.first);
	}
	
	@Test
	public void isEmpty(){
		
		assertTrue("", cola.isEmpty());
	}
	
	@Test
	public void size(){
		
	
	cola.enqueue(item);
	
	assertEquals("",1,cola.size());
	
	cola.dequeue();
	
	assertEquals("",0, cola.size());
	}
	
	@Test
	public void iterator(){
		cola.enqueue(item);
		cola.enqueue(item2);
		
		Iterator<T> iterador = cola.iterator();
		
		
		
		assertTrue("x",iterador.hasNext());
		

		assertEquals("", item, iterador.Next());
	}

}
