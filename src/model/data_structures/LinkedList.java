package model.data_structures;

public class LinkedList<T extends Comparable<T>> implements ILinkedList<T>{

	  ListNode<T> primero;
	    
	    int longitud;

		

		@Override
		public T next(T elemento) {
			// TODO Auto-generated method stub
			
			boolean encontro = false;
			
			T retornar = null;
			
			ListNode<T> ant = primero;
			ListNode<T> sig = primero.darSiguiente();
			
			while(!encontro){
				
			if(ant.darElemento().equals(elemento)){
				
				encontro = true;
				retornar = sig.darElemento();
			}
			
			ant = sig;
			sig = ant.darSiguiente();
			}
			
			return retornar; 
		}

		@Override
		public T get(T elemento) {
			boolean encontro = false;

			T retornar = null;

			ListNode<T> ant = primero;
			ListNode<T> sig = primero.darSiguiente();

			while(!encontro){

				if(ant.darElemento().equals(elemento)){

					encontro = true;
					retornar = ant.darElemento();
			}
			
			ant = sig;
			sig = ant.darSiguiente();
			}
			
			return retornar; 
		}

		@Override
		public T get(int pPosicion) {
			boolean encontro = false;

			T retornar = null;

			ListNode<T> ant = primero;
			ListNode<T> sig = primero.darSiguiente();

			while(!encontro){
				
			if(ant.posicion()==pPosicion){
				
				encontro = true;
				retornar = ant.darElemento();
			}
			
			ant = sig;
			sig = ant.darSiguiente();
			}
			
			return retornar; 
		}

		@Override
		public int size() {
			// TODO Auto-generated method stub
			return longitud;
		}

		@Override
		public T getCurrent() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void add(T elemento) {
			
			ListNode<T> nuevo = new ListNode<T>(elemento);
			boolean respuesta = false;
			if(primero==null){
				
				primero = nuevo;
				
			}
			
			else if(elemento.compareTo(primero.darElemento())<0){
				
				nuevo.cambiarSiguiente(primero);
				primero = nuevo;
				primero.cambiarPosicion(0);
				
			}
			
			else if(elemento.compareTo(primero.darElemento())>0){
				
				ListNode<T> ant1 = null;
				ListNode<T> ant = primero;
				ListNode<T> act = primero.darSiguiente();
				
				while ( act != null && elemento.compareTo( act.darElemento( ) ) > 0)
				{ ant1 = act.darAnterior();
				ant = act;
				act = act.darSiguiente( );
				}
				if ( act == null || elemento.compareTo( act.darElemento( ) ) < 0)
				{ant1.cambiarSiguiente(ant);
				ant.cambiarAnterior(ant1);
				ant.cambiarSiguiente( nuevo );
				ant.cambiarPosicion(ant.darSiguiente().posicion()-1);
				nuevo.cambiarAnterior(ant);
				nuevo.cambiarSiguiente( act );
				nuevo.cambiarPosicion(act.darSiguiente().posicion()-1);
				respuesta = true;
				}

			}
			
			if ( respuesta ) { longitud++; }
			
			
		}

		@Override
		public void delete(T elemento) {
			boolean encontro = false;


			if(primero.darElemento().equals(elemento)){

				if(primero.darSiguiente()!=null){

					primero = primero.darSiguiente();
					
					longitud--;
				}

				else{

					primero = null;
					longitud--;
				}
			}

			ListNode<T> ant1 = primero;
			ListNode<T> act = primero.darSiguiente();
			ListNode<T> sig = act.darSiguiente();

			while(!encontro&&act!=null){

				if(act.darElemento().equals(elemento)){

					encontro = true;
					ant1.cambiarSiguiente(sig);
					sig.cambiarAnterior(ant1);
					longitud--;
				}
				ant1 = act;
				act = sig;
				sig = sig.darSiguiente();
			}	



		}
		
}

		