package model.vo;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

import model.data_structures.IStack;
import model.data_structures.Iterator;
import model.data_structures.Queue;
import model.data_structures.Stack;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	/**
	 * 
	 */
	private Stack<Servicio> servicios;
	
	/**
	 * 
	 */
	private Servicio top;
	
	/**
	 * 
	 */
	private String id;
	
	
	/**
	 * @return id - taxi_id
	 */
	
	public Taxi(){
		
		servicios =  new Stack<Servicio>();
	}
	
	public String getTaxiId()
	{
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return company
	 */
	public String getCompany()
	{
		// TODO Auto-generated method stub
		return "company";
	}
	
	/**
	 * 
	 */
	
	public double distancias(){
		
		Iterator<Servicio> iterador = servicios.iterador();
        int distancia = 0;
        

		while(iterador.hasNext()){
             
			 Servicio act = iterador.Next();
			 
			 distancia += act.getTripMiles();
			}

		return distancia;
	}
	/**
	 * 
	 * @return
	 */
	
	public Stack<Servicio> darServicios(){
		return servicios;
	}
	
	/**
	 * 
	 */
	
	public Servicio top(){
		return top;
	}
	
	/**
	 * 
	 */
	@Override
	public int compareTo(Taxi o) 
	{
		// TODO Auto-generated method stub
		return 0;
	}	
	
	/**
	 * 
	 */
	
	public double totalDistancias(){
		
		Iterator<Servicio> iterador = servicios.iterador();
        int distancia = 0;
        

		while(iterador.hasNext()){
             
			 Servicio act = iterador.Next();
			 
			 distancia += act.getTripMiles();
			}

		return distancia;
	}

	/**
	 * 
	 */
	
	public double totalGanacias(){
		
		Iterator<Servicio> iterador = servicios.iterador();
        int ganancia = 0;
        Servicio act = null;
        

		while(iterador.hasNext()){
             
			  act = iterador.Next();
			 
			 ganancia += act.getTripTotal();
			}

		return ganancia;
		
	}
	/**
	 * @throws ParseException 
	 * 
	 */
	
	public Queue<Servicio> serviciosEnRango(String fechaI, String fechaF) throws ParseException{
		
		Iterator<Servicio> iterador = servicios.iterador();
		
		Queue<Servicio> cola = new Queue<Servicio>();
		Servicio act = iterador.Next();
		
		while(iterador.hasNext()){
			
			 act = iterador.Next();
			
			Date inicial = convertirFecha(act.fechaI());
			Date final1 = convertirFecha(act.fechaF());
			if(inicial.compareTo(convertirFecha(fechaI))>=0||final1.compareTo(convertirFecha(fechaF))<=0){
				
				cola.enqueue(act);
			}

		}

		return cola;


	}
	
	/**
	 * @throws ParseException 
	 */
	
	public double gananciaServicio(String fechaI, String fechaF) throws ParseException{
		
		Queue<Servicio> cola = serviciosEnRango(fechaI, fechaF);
		Iterator<Servicio> iterador = cola.iterator();
		double ganancia = 0;
		Servicio act = null;

		while(iterador.hasNext()){

			 act = iterador.Next();
            ganancia+= act.getTripTotal();
			
			}

		return ganancia;
	}

	/**
	 * 
	 */

	public double distanciaServicio(String fechaI, String fechaF) throws ParseException{

		Queue<Servicio> cola = serviciosEnRango(fechaI, fechaF);
		Iterator<Servicio> iterador = cola.iterator();
		double ganancia = 0;
		Servicio act = null;

		while(iterador.hasNext()){

			 act = iterador.Next();
			ganancia+= act.getTripMiles();

		}

		return ganancia;
	}

	/**
	 * @throws ParseException 
	 * 
	 */
	
	public Date convertirFecha(String i) throws ParseException{
		
		SimpleDateFormat fecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		
		
			
			Date date = fecha.parse(i);
		
			return date;
	}

	public Stack<String> pilaServicios(String dateI, String dateF, int n) throws ParseException{
		
		Queue<Servicio> cola = serviciosEnRango(dateI, dateF);
		Stack<String> pila = new Stack<String>();
	    Iterator<Servicio> iterador = cola.iterator();
	  
	    double contadorG = 0;
	    double contadorM = 0;
	    Servicio act = null;
	    Date comparar = null;
	    Date finl1 = null;
	    
	    while(iterador.hasNext()){
	    act = iterador.Next();
	    
	    
	    Date inicl = convertirFecha(iterador.Next().fechaI());
	    Date fnl = convertirFecha(iterador.Next().fechaF());
	    
	    
	    	if(contadorM<=n){
	    		Servicio ser = cola.dequeue();
	            String x = "Distancia recorrida:" +ser.getTripMiles()+"Costo total:"+ser.getTripTotal()+"Fecha incial:"+ser.fechaI()+"Fecha final:" +ser.fechaF();    		
	    		contadorG+= act.getTripTotal(); 
	    		contadorM+= act.getTripMiles();
	    		pila.push(x);
	    	}
	    	
	    	else{
	    		if(!pila.isEmpty()){
	    		
	    	    if(inicl.before(comparar))comparar = inicl;
	    		
	    		
	    		if(fnl.after(finl1))finl1 = fnl;
	    		
	    		}
	    		else{
	    			
	    			pila.push( "Distancia recorrida:" +contadorM+"Costo total:"+contadorG+"Fecha incial:"+comparar+"Fecha final:" +finl1);    		
	        		
	    			contadorM=0;
	    			contadorG = 0;
	    			comparar = null;
	    			finl1 = null;
	    		}
	    		}
	    	}
	    	
	    return pila;
	    }


	
	
	
	
	
}