package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.Stack;

public class ZonaServicios implements Comparable<ZonaServicios>{

	private String idZona;
	
	private LinkedList<String> fechasServicios;
	
	private Stack<Servicio> serviciosZona;
	
	
	public ZonaServicios(){
		
		serviciosZona = new Stack<Servicio>();
		
	}

	public String getIdZona() {
		return idZona;
	}
	
	



	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}



	public LinkedList<String> getFechasServicios() {
		return fechasServicios;
	}



	public void setFechasServicios(LinkedList<String> fechasServicios) {
		this.fechasServicios = fechasServicios;
	}



	@Override
	public int compareTo(ZonaServicios o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
