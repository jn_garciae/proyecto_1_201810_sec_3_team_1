package controller;

import java.text.ParseException;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.cargarSistema(direccionJson);
	}
	//1A
	public static IQueue<Servicio> darServiciosEnRango(String DateF, String DateI){
		
		return manager.darServiciosEnPeriodo( DateI, DateF);
	}

	//2A
	public static Taxi darTaxiConMasServiciosEnCompania(String dateI, String dateF, String company) throws ParseException
	{
		return manager.darTaxiConMasServiciosEnCompaniaYRango(dateI,dateF, company);
	}

	//3A
	public String[] darInformacionTaxi(String id, String dateI, String dateF) throws ParseException{
    return manager.darInformacionTaxiEnRango(dateI,dateF, id );
	}

	//4A
	public static LinkedList<Servicio> darListaRangosDistancia(String horaInicial, String horaFinal) throws ParseException 
	{
		return manager.darListaRangosDistancia(horaInicial, horaFinal);
	}
	
	//1B
	public static LinkedList<Compania> darCompaniasTaxisInscritos()
	{
		return manager.darCompaniasTaxisInscritos();
	}
	
	//2B
	public static Taxi darTaxiMayorFacturacion(String dateI, String dateF, String nomCompania)
	{
		return manager.darTaxiMayorFacturacion(dateI, dateF, nomCompania);
	}
	
	//3B
	public static Servicio[] darServiciosZonaValorTotal(String dateI, String dateF, String idZona)
	{
		return manager.darServiciosZonaValorTotal(dateI, dateF, idZona);
	}
	
	//4B
	public static LinkedList<ZonaServicios> darZonasServicios (String dateI, String dateF)
	{
		return manager.darZonasServicios(dateI, dateF);
	}
	
	//2C
	public static LinkedList<Compania> companiasMasServicios(String dateI, String dateF, int n) throws ParseException
	{
		return manager.companiasMasServicios(dateI,dateF, n);
	}

	//3C
	public static LinkedList<Taxi> taxisMasRentables()
	{
		return manager.taxisMasRentables();
	}

	//4C
	public static IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha)
	{
		return manager.darServicioResumen(taxiId,horaInicial,horaFinal,fecha);
	}



}
